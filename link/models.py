from django.db import models
from django.utils import timezone
from django.urls import reverse
from django.conf import settings

# user ID -> Link ID, tag ID  
# Link ID -> title, tag, url, comment, created, updated 
# tag -> Link ID 


class Link(models.Model):
    title = models.CharField(max_length=25)
    tag = models.CharField(max_length=25)
    url = models.URLField(max_length=200)
    comment = models.CharField(max_length=300, default="", blank=True)
    created = models.DateTimeField(editable=False)
    updated = models.DateTimeField()
    
    def save(self, *args, **kwargs):
        if not self.id:
            self.created = timezone.now()
        self.updated = timezone.now()
        super(Link, self).save(*args, **kwargs)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('link_detail', args=[str(self.id)])

class Tag(models.Model):
    title = models.CharField(max_length=20)
    
    def __str__(self):
        return self.title
