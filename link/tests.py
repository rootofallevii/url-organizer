from django.contrib.auth import get_user_model
from django.test import Client, TestCase
from django.urls import reverse
from .models import Link

class LinkDetailTest(TestCase):

    def setUp(self):
        self.user = get_user_model().objects.create_user(
                username='testuser',
                email='test@email.com',
                password='secret'
        )

        self.link = Link.objects.create(
                title="Test title1",
                url="www.test1.com",
                tag="Test tag1",
                comment="Test comment1"
        )

    def test_string_representation(self):
        link = Link(title="Test title1")
        self.assertEqual(str(link), link.title)

    # Write tests here.
