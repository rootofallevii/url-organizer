# link/urls.py
from django.urls import path
from . import views

urlpatterns = [
        path('', views.LinkListView.as_view(), name='home'),
        path('link/<int:pk>/', views.LinkDetailView.as_view(), name='link_detail'),
        path('link/new/', views.LinkCreateView.as_view(), name='link_new'),
        path('link/<int:pk>/edit/', views.LinkUpdateView.as_view(), name='link_edit'),
        path('link/<int:pk>/delete/', views.LinkDeleteView.as_view(), name='link_delete'),
]
