from django.shortcuts import render
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Link
from django.urls import reverse_lazy

class LinkListView(LoginRequiredMixin, ListView):
    model = Link
    template_name = 'home.html'
    login_url = 'login'

class LinkDetailView(LoginRequiredMixin, DetailView):
    model = Link
    template_name = 'link_detail.html'
    login_url = 'login'

class LinkCreateView(LoginRequiredMixin, CreateView):
    model = Link
    template_name = 'link_new.html'
    fields = ['title', 'tag', 'url', 'comment'] 
    login_url = 'login'
    
    def form_valid(self, form):
	    form.instance.author = self.request.user
	    return super().form_valid(form)

class LinkUpdateView(LoginRequiredMixin, UpdateView):
    model = Link
    fields = ['title', 'tag', 'url', 'comment']
    template_name = 'link_edit.html'
    login_url = 'login'

class LinkDeleteView(LoginRequiredMixin, DeleteView):
    model = Link
    template_name = 'link_delete.html'
    success_url = reverse_lazy('home')
    login_url = 'login'

